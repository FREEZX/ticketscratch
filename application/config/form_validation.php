<?php 
$config = array(
	'department' => array(
		array(
			'field'   => 'name', 
			'label'   => 'Name', 
			'rules'   => 'trim|required'
		),
		array(
			'field'   => 'description', 
			'label'   => 'Description', 
			'rules'   => 'trim|required'
		),
		array(
			'field'   => 'manager', 
			'label'   => 'Password Confirmation', 
			'rules'   => 'required|is_natural_no_zero'
		),   
		array(
			'field'   => 'accountable_user', 
			'label'   => 'Accountable User', 
			'rules'   => 'required|is_natural_no_zero'
		)
	),
	'ticket' => array(
		array(
			'name'   => 'description', 
			'label'   => 'Description', 
			'rules'   => 'trim'
		)
	)
);