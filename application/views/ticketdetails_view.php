<html>
<head>
	<title>My Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset='UTF-8'>
	<!-- Bootstrap -->
	<link href="/TicketScratchNew//resources/css/bootstrap.min.css" rel="stylesheet">
	<!--Include my custom css -->
	<link href="/TicketScratchNew//resources/css/mycss.css" rel="stylesheet"></head>
<body>

	<div class="container customwrap">

		<!-- NAVBAR
================================================== -->
		<nav class="navbar navbar-default navbar-static-top " role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="ticketscratch" class="navbar-brand" href="#">TicketScratch</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li>
						<a href="#">Нов Тикет</a>
					</li>
					<li>
						<a class="active"  href="#">Мои Тикети</a>
					</li>
					<li><a href="#">Оддели</a></li>
      				<li><a href="#">Корисници</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->

		</nav>

		<div class="container fill customcontainer">

			<div class="row">
				<div class="col-md-3 well">
					<label for="tickedId" class="customddbtn">Број на тикет:</label>
					<br />
					<div class="btn-group customddbtn">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
							Статус
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="#">Low</a>
							</li>
							<li>
								<a href="#">Medium</a>
							</li>
							<li>
								<a href="#">High</a>
							</li>
						</ul>
					</div>
					<br />

					<label for="createdBy" class="customddbtn">Креиран од:</label>
					<br />
					<label for="emptyLabel" class="customddbtn"></label>
					<br />

					<div class="btn-group customddbtn">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
							Департмент
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="#">Безбедност</a>
							</li>
							<li>
								<a href="#"></a>
							</li>
						</ul>
					</div>
					<br />

					<label for="assignedTo" class="customddbtn">Одговорно лице:</label>
					<br />
					<label for="emptyLabel" class="customddbtn"></label>
					<br />

					<label for="timeCreated" class="customddbtn">Време на креирање:</label>
					<br />
					<label for="emptyLabel" class="customddbtn"></label>
					<br />

					<label for="lastChange" class="customddbtn">Последна промена:</label>
					<br />
					<label for="emptyLabel" class="customddbtn"></label>
					<br />

				</div>

				<div class="col-md-9 well">

				
					
					<!-- Modal for comments-->
					<div class="commentContainer">
						<div class="panel customPanel">
							<label for="name">
								<strong>Име</strong>
							</label>
							<label for="surname">
								<strong>Презиме</strong>
							</label>
							<label for="date" class="customDate">12 minutes ago</label>
							<br />
							<label for="emptyName"></label>
							<label for="emptySurname"></label>
							<label for="emptyDate" class="customDate"></label>
							<p>Irina e srcka.</p>
						</div>
					</div>



					

					<br />

					<div class="form-group">
						<label for="inputComment">Додадете коментар:</label>
					</div>
					<textarea class="form-control" rows="4"></textarea>
					<br />
					<button class="btn btn-primary">Додади коментар</button>
				</div>
			</div>
		</div>

		<!-- FOOTER -->
		<footer class="footer well well-sm navbar-fixed-bottom text-center customfooter">
			<p class="glyphicon glyphicon-copyright-mark">2013 TicketScratch</p>
		</footer>

	</div>
	<!-- /.container -->
</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>

</body>
</html>