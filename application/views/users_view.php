<html>
<head>
	<title>Users</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset='UTF-8'>
    <!-- Bootstrap -->
    <link href="/TicketScratchNew//resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/TicketScratchNew/resources/js/bootstrap.min.js"></script>
    <!--Include my custom css -->
    <link href="/TicketScratchNew//resources/css/mycss.css" rel="stylesheet">
</head>
<body>

<div class="container">
	
	

<!-- NAVBAR
================================================== -->
<nav class="navbar navbar-default navbar-static-top " role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a id="ticketscratch" class="navbar-brand" href="#" > TicketScratch</a>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="#">New Ticket</a></li>
      <li class="active"><a href="#">My Tickets</a></li>  
    </ul>
  </div><!-- /.navbar-collapse -->
  
  
</nav>

<div class="container fill customcontainer">
	
  	<div class="table-bordered customtable">
		<table class="table table-hover">
 			<thead class="customthead">
 				<tr><th>Username</th><th>Fisrt Name</th><th>Last Name</th><th>Type</th></tr>
 			</thead>
 			<tbody>
 				<tr>
 					<td>Stormwin</td>
 					<td>Владо</td>
 					<td>Патката</td>
 					<td>
 						<div class="btn-toolbar">
							<div class="btn-group" >
								<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
								User Type<span class="caret"></span>
								</button>
									<ul class="dropdown-menu">
										<li>
											<a href="#">User</a>
										</li>
										<li>
											<a href="#">Agent</a>
										</li>

									</ul>
							</div>
						</div>
 						
 					</td>
 				</tr>
 			</tbody>

		</table>
  		<ul class="pagination custompaging">
  				<li class="disabled"><a href="#">&laquo;</a></li>
  				<li class="active"><a href="#">1</a></li>
  
		</ul>
  	
  	</div>
  </div>




      <!-- FOOTER -->
      <footer class="footer well well-sm navbar-fixed-bottom text-center customfooter">
        <p class="glyphicon glyphicon-copyright-mark"> 2013 TicketScratch</p>
      </footer>

    </div><!-- /.container -->
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../docs-assets/js/holder.js"></script>
  



</body>
</html>