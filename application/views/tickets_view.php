<html>
<head>
	<title>My Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset='UTF-8'>
    <!-- Bootstrap -->
    <link href="/TicketScratchNew//resources/css/bootstrap.min.css" rel="stylesheet">
    <!--Include my custom css -->
    <link href="/TicketScratchNew//resources/css/mycss.css" rel="stylesheet">
</head>
<body>

<div class="container customwrap">
	
	

<!-- NAVBAR
================================================== -->
<nav class="navbar navbar-default navbar-static-top " role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a id="ticketscratch" class="navbar-brand" href="#"> TicketScratch</a>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="#">Нов Тикет</a></li>
      <li class="active"><a href="#">Мои Тикети</a></li> 
      <li><a href="#">Оддели</a></li>
      <li><a href="#">Корисници</a></li>
    </ul>
  </div><!-- /.navbar-collapse -->
  
  
</nav>

<div class="container fill customcontainer">
	
  	
		<div class="row">
			<div class="col-md-3 well">
				<div class="customa"><a>Отворени</a></div><br />
				<div class="customa"><a>Затворени</a></div><br />
				<div class="customa"><a>Повторно отворени</a></div><br />
				<div class="customa"><a>Мои Тикети</a></div><br />
				<div class="customa"><a>Доделени</a></div><br />
				<div class="customa"><a>Задоцнети</a></div><br />
			</div>

			<div class="col-md-9 customrow">
				<div class=" table-bordered customtable" >
							<table class="table table-hover" >
								<thead class="customthead">
									<tr>
										<th>Име</th><th>Статус</th><th>Оддел</th><th>Одговорно лице</th><th>Дата</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Проблем со конекција</td><td>Активен</td><td>оддел 1</td><td>Владо Величковски</td><td>05.11.2010</td>
									</tr>
									

								</tbody>

							</table>
							<ul class="pagination">
								<li class="disabled">
									<a href="#">&laquo;</a>
								</li>
								<li class="active">
									<a href="#">1</a>
								</li>

							</ul>

						</div>

				
			</div>
  </div>




      <!-- FOOTER -->
      <footer class="footer well well-sm navbar-fixed-bottom text-center customfooter">
        <p class="glyphicon glyphicon-copyright-mark"> 2013 TicketScratch</p>
      </footer>

    </div><!-- /.container -->
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  



</body>
</html>