<html>
<head>
	<title>My Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset='UTF-8'>
  <base href="<?php echo base_url(); ?>">
  <!-- Bootstrap -->
  <link href="resources/css/bootstrap.min.css" rel="stylesheet">
  <!--Include my custom css -->
  <link href="resources/css/mycss.css" rel="stylesheet">
  <script type="text/javascript">var base_url = "<?php echo base_url();  ?>"</script>
  <script src="resources/js/lib/jquery.min.js"></script>
  <script src="resources/js/lib/bootstrap.min.js"></script>
  <script src="resources/js/lib/handlebars.min.js"></script>
  <script src="resources/js/lib/ember.js"></script>
  <script src="resources/js/lib/ember-data.js"></script>
  <script src="resources/js/lib/moment.min.js"></script>
  <script src="resources/js/client.js"></script>
  <script src="resources/js/models.js"></script>
  <script src="resources/js/wizard.js"></script>
  <script src="resources/js/emberplugins.js"></script>
</head>
<body>
<div class="modal bs-modal-lg" id="wizardmodal" tabindex="-1" role="dialog" aria-hidden="true"></div>
<script type="text/x-handlebars">
  <nav class="navbar navbar-default navbar-static-top " role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      {{#link-to 'newTicket' class="navbar-brand" id="ticketscratch"}}TicketScratch{{/link-to}}
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        {{#link-to 'newTicket' tagName="li"}}<a {{bind-attr href="view.href"}}>New Ticket</a>{{/link-to}}
        <?php if($this->session->userdata('id') !== FALSE){ ?>
          {{#link-to 'myTickets' tagName="li"}}<a {{bind-attr href="view.href"}}>My Tickets</a>{{/link-to}}
        <?php } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
  </nav>
  <div class="container">
  {{outlet}}
  </div>
  <div style="width:100%; height:70px;"></div>
  <!-- FOOTER -->
  <footer class="footer well well-sm navbar-fixed-bottom text-center customfooter">
    <p class="glyphicon glyphicon-copyright-mark"> 2014 TicketScratch</p>
  </footer>
</script>


<script type="text/x-handlebars" id="newTicket">
  <div class="container fill customcontainer" >
    <div class="row well well-lg ">
      <div class="page-header page_header" align="top">
      <h2>Choose <small>how to create your ticket</small></h2>

      </div>
      <button class="btn btn-primary btn-lg col-md-offset-2 col-md-3 custombtn"> 
        Create ticket with CAS
        </br>
        </br>
        <span class="glyphicon glyphicon glyphicon-log-in customglyphicon"></span>
      </button>
      <button class="btn btn-primary btn-lg col-md-offset-2 col-md-3 custombtn createwithemail"> 
        Create ticket with E-Mail
        </br>
        </br>
        <span class="glyphicon glyphicon-envelope customglyphicon"></span>
      </button>
    </div>
  </div>
</script>

<script type="text/x-handlebars" id="myTickets">
<div class="container fill customcontainer">
  <div class="row table-bordered customtable" >
    <table class="table table-hover" >
      <thead class="customthead">
        <tr><th>Name</th><th>Status</th><th>Created</th><th>Updated</th></tr>
      </thead>
      <tbody>
        {{#each model}}
        <tr {{action 'openDetails' id}}><td>{{info}}</td><td>{{status}}</td><td>{{date time_created}}</td><td>{{date time_updated}}</td></tr>
        {{/each}}
      </tbody>
    </table>
    {{#if hasMore}}
      {{#if loadingMore}}
        <h4>Loading more...</h4>
      {{else}}
        <a href='#' {{action 'getMore'}}><h4>Get More...</h4></a>
      {{/if}}
    {{/if}}
  </div>
</div>
</script>

<script id="ticket" type="text/x-handlebars">
  <div class="container fill customcontainer">
    <div class="row">
      <div class="col-md-3 well">
        <label for="tickedId" class="customddbtn">Ticket #{{id}}</label>
        <br />
        <div class="btn-group customddbtn">
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            Статус
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li>
              <a href="#">Low</a>
            </li>
            <li>
              <a href="#">Medium</a>
            </li>
            <li>
              <a href="#">High</a>
            </li>
          </ul>
        </div>
        <br />

        <label for="createdBy" class="customddbtn">Креиран од:</label>
        <br />
        <label for="emptyLabel" class="customddbtn"></label>
        <br />

        <div class="btn-group customddbtn">
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            Департмент
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li>
              <a href="#">Безбедност</a>
            </li>
            <li>
              <a href="#"></a>
            </li>
          </ul>
        </div>
        <br />

        <label for="assignedTo" class="customddbtn">Accountable user:</label>
        <br />
        <label for="emptyLabel" class="customddbtn"></label>
        <br />

        <label for="timeCreated" class="customddbtn">Creation date:</label>
        <br />
        <label for="emptyLabel" class="customddbtn">{{date time_created}}</label>
        <br />

        <label for="lastChange" class="customddbtn">Last change:</label>
        <br />
        <label for="emptyLabel" class="customddbtn">{{date time_updated}}</label>
        <br />

      </div>

      <div class="col-md-9 well">

      
        
        <!-- Modal for comments-->
        {{#each timeline}}
          <div class="commentContainer">
            <div class="panel customPanel">
              <label for="name">
                <strong>{{change_user.fullname}}</strong>
              </label>
              <label for="date" class="customDate">{{date time_change}}</label>
              <br />
              <p>{{change_data}}</p>
            </div>
          </div>
          <br />
        {{/each}}

        <div class="form-group">
          <label for="inputComment">Add comment:</label>
        </div>
        <textarea class="form-control" id="newComment" rows="4"></textarea>
        <br />
        <button {{action 'addComment'}} class="btn btn-primary">Add comment</button>
      </div>
    </div>
  </div>
</script>


<script type="text/x-handlebars" data-template-name="bootstrapDropdown">
    
{{#view categorySelect tagName="div" class="btn-group dropdown" classBinding="isOpen:open"}}
  <a class="btn dropdown-toggle btn-small" data-toggle="dropdown" href="#">
    {{selected.title}}
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
    {{#each content}}
    <li {{bindAttr class="isActive:active"}}><a href="#" {{action "select" on="click"}}>{{title}}</a></li>
    {{/each}}
  </ul>
{{/view}}
    
</script>

<script type="text/x-handlebars-template" id="wizardguidetemplate">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Wizzard Guide</h4>
    </div>
    <div class="modal-body">
      <h3>{{question}}</h3>
      {{#each answers}}
        <div class="radio">
          <label>
            <input type="radio" name="answer" value="{{answer_id}}" {{#equal answer_id ../lastanswer}}checked{{/equal}}>
            {{answer_text}}
          </label>
        </div>
      {{/each}}
    </div>
    <div class="modal-footer">
    {{#if hasback}}
      <button type="button" class="btn btn-default wizardback">Back</button>
    {{/if}}
      <button type="button" class="btn btn-primary wizardnext">Next</button>
    </div>
  </div>
</div>
</script>
<script type="text/x-handlebars-template" id="wizardguidefinishtemplate">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Wizzard Guide</h4>
    </div>
    <div class="modal-body">
      <h3>Enter optional information</h3>
      <textarea id="description" class="form-control"></textarea>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default wizardback">Back</button>
      <button type="button" class="btn btn-danger wizardfinish">Finish</button>
    </div>
  </div>
</div>
</script>

</html>