<!doctype html>
<html>
<head>
	<base href="<?php echo base_url('resources').'/'; ?>"/>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<script src="js/lib/jquery.jsPlumb-1.5.5-min.js "></script>
	<link rel="stylesheet" type="text/css" href="css/jsplumb.css">
	<script>
	//
// this file is just used to automate the process of adding links to the individual demos to their previous/next
// demo.  you don't need to concern yourself with what's going on in here.
//
		jsPlumb.ready(function() {

		var instance = jsPlumb.getInstance({
			// default drag options
			DragOptions : { cursor: 'pointer', zIndex:2000 },
			// the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
			// case it returns the 'labelText' member that we set on each connection in the 'init' method below.
			ConnectionOverlays : [
				[ "Arrow", { location:1 } ],
				[ "Label", { 
					location:0.6,
					id:"label",
					cssClass:"aLabel"
				}]
			],
			Container:"flowchart-demo"
		});		

		// this is the paint style for the connecting lines..
		var connectorPaintStyle = {
			lineWidth:3,
			strokeStyle:"#5bc0de",
			joinstyle:"round",
			outlineColor:"white",
			outlineWidth:2
		},
		// .. and this is the hover style. 
		connectorHoverStyle = {
			lineWidth:3,
			outlineWidth:2,
			outlineColor:"white",
			strokeStyle:"#3498db"
		},
		targetEndpointHoverStyle = {
			fillStyle:"#3498db"
		},
		sourceEndpointHoverStyle = {
			strokeStyle:"#3498db"
		},
		// the definition of source endpoints (the small blue ones)
		sourceEndpoint = {
			endpoint:"Dot",
    		anchor:[ "BottomCenter", { shape:"Square", anchorCount:150 }],
			paintStyle:{ 
				strokeStyle:"#5bc0de",
				fillStyle:"transparent",
				radius:7,
				lineWidth:3
			},
			isSource:true,
			maxConnections: 10,
			connector:[ "Flowchart", { stub:[30, 30], gap:8, cornerRadius:10, alwaysRespectStubs:true }],
			connectorStyle:connectorPaintStyle,
			hoverPaintStyle:sourceEndpointHoverStyle,
			connectorHoverStyle:connectorHoverStyle,
            dragOptions:{},
            overlays:[
            	[ "Label", {
                	location:[0.5, 1.5],
                	label:"Drag",
                	cssClass:"endpointSourceLabel" 
                } ]
            ]
		},
		// the definition of target endpoints (will appear when the user drags a connection) 
		targetEndpoint = {
			endpoint:"Dot",
			paintStyle:{ fillStyle:"#5bc0de",radius: 8 },
			hoverPaintStyle:targetEndpointHoverStyle,
			maxConnections:-1,
			dropOptions:{ hoverClass:"hover", activeClass:"active" },
			isTarget:true,
            overlays:[
            	[ "Label", { location:[0.5, -0.5], label:"Drop", cssClass:"endpointTargetLabel" } ]
            ]
		},			
		init = function(connection) {			
			connection.getOverlay("label").setLabel(connection.sourceId.substring(15) + "-" + connection.targetId.substring(15));
			connection.bind("editCompleted", function(o) {
				if (typeof console != "undefined")
					console.log("connection edited. path is now ", o.path);
			});
		};			

		var _addEndpoints = function(toId, sourceAnchors, targetAnchors) {
				for (var i = 0; i < sourceAnchors.length; i++) {
					var sourceUUID = toId + sourceAnchors[i];
					instance.addEndpoint("flowchart" + toId, sourceEndpoint, { anchor:sourceAnchors[i], uuid:sourceUUID });						
				}
				for (var j = 0; j < targetAnchors.length; j++) {
					var targetUUID = toId + targetAnchors[j];
					instance.addEndpoint("flowchart" + toId, targetEndpoint, { anchor:targetAnchors[j], uuid:targetUUID });						
				}
			};

		// suspend drawing and initialise.
		instance.doWhileSuspended(function() {

			_addEndpoints("Window1", ["BottomCenter"], ["TopCenter"]);
			_addEndpoints("Window2", ["BottomCenter"], ["TopCenter"]);
			_addEndpoints("Window3", ["BottomCenter"], ["TopCenter"]);
			_addEndpoints("Window4", ["BottomCenter"], ["TopCenter"]);					
			// listen for new connections; initialise them the same way we initialise the connections at startup.
			instance.bind("connection", function(connInfo, originalEvent) { 
				init(connInfo.connection);
			});			
						
			// make all the window divs draggable						
			instance.draggable(jsPlumb.getSelector(".flowchart-demo .window"), { grid: [20, 20] });		
			// THIS DEMO ONLY USES getSelector FOR CONVENIENCE. Use your library's appropriate selector 
			// method, or document.querySelectorAll:
			//jsPlumb.draggable(document.querySelectorAll(".window"), { grid: [20, 20] });
	        
			// connect a few up
			// instance.connect({uuids:["Window2BottomCenter", "Window3TopCenter"], editable:true});
			// instance.connect({uuids:["Window2LeftMiddle", "Window4LeftMiddle"], editable:true});
			// instance.connect({uuids:["Window4TopCenter", "Window4RightMiddle"], editable:true});
			// instance.connect({uuids:["Window3RightMiddle", "Window2RightMiddle"], editable:true});
			// instance.connect({uuids:["Window4BottomCenter", "Window1TopCenter"], editable:true});
			// instance.connect({uuids:["Window3BottomCenter", "Window1BottomCenter"], editable:true});
			//
	        
			//
			// listen for clicks on connections, and offer to delete connections on click.
			//
			instance.bind("click", function(conn, originalEvent) {
				if (confirm("Delete connection from " + conn.sourceId + " to " + conn.targetId + "?"))
					jsPlumb.detach(conn); 
			});	
			
			instance.bind("connectionDrag", function(connection) {
				console.log("connection " + connection.id + " is being dragged. suspendedElement is ", connection.suspendedElement, " of type ", connection.suspendedElementType);
			});		
			
			instance.bind("connectionDragStop", function(connection) {
				console.log("connection " + connection.id + " was dragged");
			});

			instance.bind("connectionMoved", function(params) {
				console.log("connection " + params.connection.id + " was moved");
			});
		});
		
	});
	</script>
</head>
<body data-demo-id="flowchart" data-library="jquery">
        <div id="main">    
            <div class="demo flowchart-demo" id="flowchart-demo">
                <div class="window" id="flowchartWindow1"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ipsum massa, ultrices a lacinia vitae, volutpat vel massa. Integer eleifend rutrum velit at scelerisque. Morbi pretium, purus hendrerit cursus molestie, enim elit rhoncus nunc, eu volutpat risus tellus id velit. Nulla quis cursus est. Praesent dui nisl, cursus eget elit vitae, viverra auctor sapien. Vivamus vitae augue sagittis, vestibulum enim eu, rhoncus lectus. Donec ac pharetra nunc, ac rutrum felis. Phasellus porta facilisis lacus, nec semper eros pharetra id. In hac habitasse platea dictumst. Vivamus pellentesque ultrices nisl, at iaculis lectus rutrum vestibulum. Donec ut ante arcu. Pellentesque ullamcorper et erat at semper.</strong><br/><br/></div>
                <div class="window" id="flowchartWindow2"><strong>2</strong><br/><br/></div>
                <div class="window" id="flowchartWindow3"><strong>3</strong><br/><br/></div>
                <div class="window" id="flowchartWindow4"><strong>4</strong><br/><br/></div>                     
            </div>          
            
        </div>
        
    </body>
</html>