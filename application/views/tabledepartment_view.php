<html>
<head>
	<title>My Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset='UTF-8'>
  <base href="<?php echo base_url();?>">
  <!-- Bootstrap -->
  <link href="resources/css/bootstrap.min.css" rel="stylesheet">
  <!--Include my custom css -->
  <link href="resources/css/mycss.css" rel="stylesheet">

  <script src="resources/js/lib/jquery.min.js"></script>
  <script src="resources/js/lib/bootstrap.min.js"></script>
  <script src="resources/js/lib/handlebars.min.js"></script>
  <script src="resources/js/lib/ember.js"></script>
  <script src="resources/js/lib/ember-data.js"></script>
  <script src="resources/js/lib/moment.min.js"></script>
  <script src="resources/js/emberplugins.js"></script>
  <script src="resources/js/client.js"></script>
</head>
<body>

<div class="container">
	
	

<!-- NAVBAR
================================================== -->
<nav class="navbar navbar-default navbar-static-top " role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a id="ticketscratch" class="navbar-brand" href="#" > TicketScratch</a>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li><a href="#">New Ticket</a></li>
      <li><a href="#">My Tickets</a></li>  
      <li class="active"><a href="#">Departments</a></li> 
    </ul>
  </div><!-- /.navbar-collapse -->
  
  
</nav>
	<div class="customdiv">
		<button type="button" class="btn btn-success pull-right customaddbtn" data-toggle="modal" data-target="#addModal">
			<span class="glyphicon glyphicon-plus"></span> Add
		</button>
	</div>

<div class="container fill customcontainer">
	
  	<div class="table-bordered customtable">
		<table class="table table-hover">
		<thead class="customthead">
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Manager</th>
				<th>Accountable user</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Test test</td>
				<td>Test1</td>
				<td>Testiranje</td>
				<td>Testing</td>
				<td>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
						<span class="glyphicon glyphicon-edit"></span> Edit
					</button>
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
						<span class="glyphicon glyphicon-trash"></span> Delete
					</button>

					<!-- Modal for edit Button-->
					<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="myModalLabel">Editing</h4>
								</div>
								<div class="modal-body">
									<form class="form-horizontal" role="form">
										<div class="form-group">
											<label for="inputName" class="col-sm-2 control-label">Name</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputName" placeholder="Name">
											</div>
										</div>
										<div class="form-group">
											<label for="inputDescription" class="col-sm-2 control-label">Description</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputPassword3" placeholder="Description">
											</div>
										</div>
										<div class="form-group">
											<label for="inputManager" class="col-sm-2 control-label">Manager</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputManager" placeholder="Manager">
											</div>
										</div>
										<div class="form-group">
											<label for="inputAccountableUser" class="col-sm-2 control-label">Accountable User</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputAccountableUser" placeholder="Accountable User">
											</div>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save changes</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->

					<!-- Modal for delete Button-->
					<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="myModalLabel">Deleting</h4>
								</div>
								<div class="modal-body">
									<form class="form-horizontal" role="form">
										<div class="row">
											<p class="text-center"><strong>Are you sure you want to delete this item?</strong></p>
										</div>
									</form>	
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary">Yes</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->

					<!--Modal for add Button-->
					<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="myModalLabel">Adding a new department</h4>
								</div>
								<div class="modal-body">
									<form class="form-horizontal" role="form">
										<div class="form-group">
											<label for="inputName" class="col-sm-2 control-label">Name</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputName" placeholder="Name">
											</div>
										</div>
										<div class="form-group">
											<label for="inputDescription" class="col-sm-2 control-label">Description</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputPassword3" placeholder="Description">
											</div>
										</div>
										<div class="form-group">
											<label for="inputManager" class="col-sm-2 control-label">Manager</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputManager" placeholder="Manager">
											</div>
										</div>
										<div class="form-group">
											<label for="inputAccountableUser" class="col-sm-2 control-label">Accountable User</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="inputAccountableUser" placeholder="Accountable User">
											</div>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Add a department</button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</td>		
			</tr>
		</tbody>
	</table>
  		<ul class="pagination custompaging">
  				<li class="disabled"><a href="#">&laquo;</a></li>
  				<li class="active"><a href="#">1</a></li>
  
		</ul>
  	
  	</div>
  </div>




      <!-- FOOTER -->
      <footer class="footer well well-sm navbar-fixed-bottom text-center customfooter">
        <p class="glyphicon glyphicon-copyright-mark"> 2013 TicketScratch</p>
      </footer>

    </div><!-- /.container -->
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../docs-assets/js/holder.js"></script>
  



</body>
</html>