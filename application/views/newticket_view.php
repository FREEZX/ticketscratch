<html>
<head>
	<title>New Ticket</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="/TicketScratchNew//resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/TicketScratchNew/resources/js/bootstrap.min.js"></script>
    <link href="/TicketScratchNew//resources/css/mycss.css" rel="stylesheet">
</head>
<body>

<div class="container">
	
	

<!-- NAVBAR
================================================== -->
<nav class="navbar navbar-default navbar-static-top " role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a id="ticketscratch" class="navbar-brand" href="#"> TicketScratch</a>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">New Ticket</a></li>
      <li><a href="#">My Tickets</a></li>  
    </ul>
  </div><!-- /.navbar-collapse -->
  
  
</nav>

<div class="container fill customcontainer" >
	
  	<div class="row well well-lg ">
	<div class="page-header page_header" align="top">
  		<h2>Choose <small>how to create your ticket</small></h2>
  		
	</div>
  	<button class="btn btn-primary btn-lg col-md-offset-2 col-md-3 custombtn createwithcas"> 
  		Create ticket with CAS
  		</br>
  		</br>
  		<span class="glyphicon glyphicon glyphicon-log-in customglyphicon"></span>
  		
  	</button >
  	<button class="btn btn-primary btn-lg col-md-offset-2 col-md-3 custombtn createwithmail"> 
  		Create ticket with E-Mail
  		</br>
  		</br>
  		<span class="glyphicon glyphicon-envelope customglyphicon"></span>
  	</button>
  	
  	</div>
  </div>




      <!-- FOOTER -->
      <footer class="footer well well-sm navbar-fixed-bottom text-center customfooter">
        <p class="glyphicon glyphicon-copyright-mark"> 2013 TicketScratch</p>
      </footer>

    </div><!-- /.container -->
</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../docs-assets/js/holder.js"></script>
  



</body>
</html>