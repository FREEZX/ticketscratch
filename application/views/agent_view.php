<html>
<head>
	<title>My Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset='UTF-8'>
  <base href="<?php echo base_url();?>">
  <!-- Bootstrap -->
  <link href="resources/css/bootstrap.min.css" rel="stylesheet">
  <!--Include my custom css -->
  <link href="resources/css/mycss.css" rel="stylesheet">
  <link href="resources/css/jsplumb.css" rel="stylesheet">

  <script src="resources/js/lib/jquery.min.js"></script>
  <script src="resources/js/lib/jquery-ui.min.js "></script>
  <script src="resources/js/lib/jquery.jsPlumb-1.5.5-min.js "></script>
  <script src="resources/js/lib/bootstrap.min.js"></script>
  <script src="resources/js/lib/handlebars.min.js"></script>
  <script src="resources/js/lib/ember.js"></script>
  <script src="resources/js/lib/ember-data.js"></script>
  <script src="resources/js/lib/moment.min.js"></script>
  <script src="resources/js/lib/underscore.js"></script>
  <script src="resources/js/jsplumb.js "></script>
  <script src="resources/js/agent.js"></script>
  <script src="resources/js/emberplugins.js"></script>
  <script src="resources/js/models.js"></script>
</head>
<body>

<script type="text/x-handlebars">
  <nav class="navbar navbar-default navbar-static-top " role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      {{#link-to 'category' 'open' class="navbar-brand" id="ticketscratch"}}TicketScratch{{/link-to}}
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        {{#link-to 'category' 'open' tagName="li"}}<a {{bind-attr href="view.href"}}>Tickets</a>{{/link-to}}
      </ul>
      <ul class="nav navbar-nav pull-right">
        <?php if($this->session->userdata('user_type') === 'AGENT') { ?>
          {{#link-to 'departments' tagName="li" }}<a {{bind-attr href="view.href"}}><span class="glyphicon glyphicon-cog" style="font-size: 20px"></span></a>{{/link-to}}
        <?php } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
  </nav>
  <div class="container">
  {{outlet}}
  </div>
  <div style="width:100%; height:70px;"></div>
  <!-- FOOTER -->
  <footer class="footer well well-sm navbar-fixed-bottom text-center customfooter">
    <p class="glyphicon glyphicon-copyright-mark"> 2014 TicketScratch</p>
  </footer>
</script>


<script type="text/x-handlebars" id="options">
  <div class="row">
    <div class="col-md-2">
      <div class="well">
        <ul class="nav nav-pills nav-stacked">
        {{#link-to 'departments' tagName="li"}}<a {{bind-attr href="view.href"}}>Departments</a>{{/link-to}}
        {{#link-to 'users' tagName="li"}}<a {{bind-attr href="view.href"}}>Users</a>{{/link-to}}
        {{#link-to 'system' tagName="li"}}<a {{bind-attr href="view.href"}}>System</a>{{/link-to}}
        {{#link-to 'wizard' tagName="li"}}<a {{bind-attr href="view.href"}}>Wizard</a>{{/link-to}}
        </ul>
      </div>
    </div>

    {{outlet}}
    {{outlet "sidebar"}}
  </div>
</script>


<script type="text/x-handlebars" id="wizardSidebar">
  <div class="well col-md-2 pull-right">
    <div class="row form-group">
        <strong>New question</strong>
        <textarea class="form-control wizard-textarea" id="newNodeContent"></textarea>
        <button {{action 'addWizardNode'}} type="button" class="form-control btn-success">Add Question</button>
    </div>
    <div class="row form-group">
      <strong>Answers</strong>
      <ul class="nav nav-pills nav-stacked" id="answersList">
        {{#each model.answers}}
          <li {{action 'chooseAnswer' this}} {{bind-attr data-id=id}}><a href="#">{{answer}}</a></li>
        {{/each}}
      </ul>
    </div>
    <div class="row form-group">
      <input type="text" id="newAnswer" class="form-control"></input>
      <button {{action 'addAnswer'}} type="button" class="form-control btn-success">Add Answer</button>
    </div>
    <div class="row form-group">
      <button {{action 'saveWizard'}} type="button" class="form-control btn-primary">Save</button>
    </div>
  </div>
</script>

<script type="text/x-handlebars" id="departments">
  <div class="col-md-10 customrow">
    <div class="customdiv">
      <button type="button" class="btn btn-success pull-right customaddbtn" data-toggle="modal" data-target="#addModal">
        <span class="glyphicon glyphicon-plus"></span> Add
      </button>
    </div>

    <div class="container fill customcontainer">

      <div class="table-bordered customtable">
        <table class="table table-hover">
          <thead class="customthead">
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Manager</th>
              <th>Accountable user</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {{#each model}}
              <tr>
                <td>{{name}}</td>
                <td>{{description}}</td>
                <td>{{manager.firstname}} {{manager.lastname}}</td>
                <td>{{accountable_user.firstname}} {{accountable_user.lastname}}</td>
                <td>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
                    <span class="glyphicon glyphicon-edit"></span> Edit
                  </button>
                  <button {{action 'startDelete' this}} type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                    <span  class="glyphicon glyphicon-trash"></span> Delete
                  </button>
                </td>   
              </tr>
            {{/each}}
          </tbody>
        </table>

        <!-- Modal for edit Button-->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Editing</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" placeholder="Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputDescription" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPassword3" placeholder="Description">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputManager" class="col-sm-2 control-label">Manager</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputManager" placeholder="Manager">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputAccountableUser" class="col-sm-2 control-label">Accountable User</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputAccountableUser" placeholder="Accountable User">
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Modal for delete Button-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Deleting</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" role="form">
                  <div class="row">
                    <p class="text-center"><strong>Are you sure you want to delete this item?</strong></p>
                  </div>
                </form> 
              </div>
              <div class="modal-footer">
                <button {{action 'deleteDepartment' deleteItem}} type="button" class="btn btn-primary">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!--Modal for add Button-->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Adding a new department</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" role="form">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      {{view Ember.TextField placeholder="Name" classNames="form-control" valueBinding=newName}}
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputDescription" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                      {{view Ember.TextField placeholder="Description" classNames="form-control" valueBinding=newDescription}}
                      <!-- <input type="text" class="form-control" id="inputPassword3" placeholder="Description"> --> 
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputManager" class="col-sm-2 control-label">Manager</label>
                    <div class="col-sm-10">
                      {{view Ember.Select
                        prompt="Please select the accountable user"
                        content=agents
                        optionValuePath="content.id"
                        optionLabelPath="content.fullname"
                        value="content.id"
                        selectionBinding="selectedManager"
                        classNames="form-control"
                      }}
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputAccountableUser" class="col-sm-2 control-label">Accountable User</label>
                    <div class="col-sm-10">
                      {{view Ember.Select
                        prompt="Please select the accountable user"
                        content=agents
                        optionValuePath="content.id"
                        optionLabelPath="content.fullname"
                        value="content.id"
                        selectionBinding="selectedAccountable"
                        classNames="form-control"
                      }}
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button {{action 'addDepartment'}} type="button" class="btn btn-primary">Add a department</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
      </div>
    </div>
  </div>

</script>

<script type="text/x-handlebars" id="users">
  <div class="col-md-10 customrow">
  <div class="table-bordered customtable">
    <table class="table table-hover">
      <thead class="customthead">
        <tr><th>Username</th><th>Fisrt Name</th><th>Last Name</th><th>Type</th></tr>
      </thead>
      <tbody>
        {{#each model}}
        <tr>
          <td>{{username}}</td>
          <td>{{firstname}}</td>
          <td>{{lastname}}</td>
          <td>
            {{view App.ClientTypeSelectView
              user=this
              content=controller.userTypes
              value=user_type
              optionValuePath="content.user_type"
              optionLabelPath="content.content"
              classNames="form-control"
            }}
          </td>
        </tr>
        {{/each}}
      </tbody>

      </table>    
    </div>
  </div>
</script>

<script type="text/x-handlebars" id="system">
  <div class="col-md-7 customrow">
    <form role="form">
      <div class="form-group">
        <label>Select System Manager</label>
        {{view Ember.Select
          prompt="Please select a manager user"
          content=model.users
          optionValuePath="content.id"
          optionLabelPath="content.fullname"
          value="content.id"
          selectionBinding="selectedManager"
          classNames="form-control"
        }}
      </div>
      <div class="form-group">
        <label>Select Proxy</label>
        {{view Ember.Select
          prompt="Please select a proxy user"
          content=model.users
          optionValuePath="content.id"
          optionLabelPath="content.fullname"
          value="content.id"
          selectionBinding="selectedProxy"
          classNames="form-control"
        }}
      </div>
      
      <button {{action 'save'}} type="submit" class="btn btn-default pull-right">    Save    </button>
    </form>
  </div>
</script>

<script type="text/x-handlebars" id="wizard">
  <div class="col-md-7 customrow">
    <div class="" data-demo-id="flowchart" data-library="jquery">
      <div id="main">    
        <div class="demo flowchart-demo" id="flowchart-demo">
          <div class="window" id="start"><strong>START</strong><br/><br/></div>  
          <div class="window" id="end"><strong>END</strong><br/><br/></div>  
        </div>
      </div>
    </div>
  </div>
</script>


<script type="text/x-handlebars" id="tickets">
  <div class="row">
	<div class="col-md-3 well">
    <ul class="nav nav-pills nav-stacked">
          <?php if($this->session->userdata('proxy')){ ?>
            {{#link-to 'category' 'unassigned_department' tagName="li"}}<a {{bind-attr href="view.href"}}>Not assigned to Department</a>{{/link-to}}
            {{#link-to 'category' 'assigned_department' tagName="li"}}<a {{bind-attr href="view.href"}}>Assigned to Department</a>{{/link-to}}
          <?php }
          else if($this->session->userdata('user_type') == 'AGENT'){ ?>
            {{#link-to 'category' 'my_tickets' tagName="li"}}<a {{bind-attr href="view.href"}}>My Tickets</a>{{/link-to}}
            {{#link-to 'category' 'overdue' tagName="li"}}<a {{bind-attr href="view.href"}}>Overdue</a>{{/link-to}}
            {{#link-to 'category' 'open' tagName="li"}}<a {{bind-attr href="view.href"}}>Open</a>{{/link-to}}
            {{#link-to 'category' 'closed' tagName="li"}}<a {{bind-attr href="view.href"}}>Closed</a>{{/link-to}}
            {{#link-to 'category' 'reopened' tagName="li"}}<a {{bind-attr href="view.href"}}>Reopened</a>{{/link-to}}
            {{#link-to 'category' 'info_needed' tagName="li"}}<a {{bind-attr href="view.href"}}>Info Needed</a>{{/link-to}}
            {{#link-to 'category' 'assgned_user' tagName="li"}}<a {{bind-attr href="view.href"}}>Assigned</a>{{/link-to}}
            {{#link-to 'category' 'unassigned' tagName="li"}}<a {{bind-attr href="view.href"}}>Unassigned</a>{{/link-to}}
          <?php } ?>
    <ul>
	</div>

	<div class="col-md-9 customrow">
		{{outlet}}
		
	</div>
  </div>
</script>

<script type="text/x-handlebars" id="category">
  <div class=" table-bordered customtable" >
    <table class="table table-hover" >
      <thead class="customthead">
        <tr><th>Name</th><th>Status</th><th>Created</th><th>Updated</th></tr>
      </thead>
      <tbody>
        {{#each ticket in model}}
          {{#link-to 'ticket' ticket.id tagName='tr'}}<td>{{ticket.info}}</td><td>{{ticket.status}}</td><td>{{date ticket.time_created}}</td><td>{{date ticket.time_updated}}</td>{{/link-to}}
        {{/each}}
      </tbody>
    </table>
    {{#if hasMore}}
      {{#if loadingMore}}
        <h4>Loading more...</h4>
      {{else}}
        <a href='#' {{action 'getMore'}}><h4>Get More...</h4></a>
      {{/if}}
    {{/if}}
  </div>

</script>

<script id="ticket" type="text/x-handlebars">
  <div class="container fill customcontainer">
    <div class="row">
      <div class="col-md-3 well">
        <h3>Ticket #{{id}}</h3>
        <br />
        <div>
          <label for="department">Status:</label><br/>
          {{view App.StatusSelectView
            content=statuses
            optionValuePath="content.value"
            optionLabelPath="content.content"
            value=this.status
            classNames="form-control"
          }}
        </div>

        <div>
          <label for="department">Priority:</label>
          <br/>
          {{view App.PrioritySelectView
            content=priorities
            optionValuePath="content.value"
            optionLabelPath="content.content"
            value=this.priority
            classNames="form-control"
          }}
        </div>
        <br />

        <label for="createdBy" class="customddbtn">Created by:</label>
        <br />
        {{#if user_id}}
          {{user_id.fullname}}
        {{else}}
          {{user_email}}
        {{/if}}
        <label for="emptyLabel" class="customddbtn"></label>
        <br />

        <label for="department">Department:</label>
        <br/>
        <?php
          if($this->session->userdata('proxy')){
            ?>
            {{view App.DepartmentSelectView
              content=departments
              optionValuePath="content.id"
              optionLabelPath="content.name"
              value=priority
              selectionBinding="assigned_department_id"
              classNames="form-control"
            }}
            <?php
          }
          else{
            ?>
            {{#if assigned_department}}
              {{assigned_department.name}}
            {{else}}
              NOT ASSIGNED
            {{/if}}
            <?php
          }
        ?>
        <br/>
        

        <label for="assignedTo" class="customddbtn">Assigned user:</label>
        <br/>
        <?php
          if($this->session->userdata('manager')){

          }
        ?>
        {{assigned_user_id.fullname}}

        <br />
        <label for="emptyLabel" class="customddbtn"></label>
        <br />

        <label for="timeCreated" class="customddbtn">Creation date: {{date time_created}}</label>
        <br />

        <label for="lastChange" class="customddbtn">Last change: {{date time_updated}}</label>
        <br />

      </div>

      <div class="col-md-9 well">

      
        
        <!-- Modal for comments-->
        {{#each timeline}}
          <div class="commentContainer">
            <div class="panel customPanel">
              <label for="name">
                <strong>{{change_user.fullname}}</strong>
              </label>
              <label for="date" class="customDate">{{date time_change}}</label>
              <br />
              <p>{{change_data}}</p>
            </div>
          </div>
          <br />
        {{/each}}

        <div class="form-group">
          <label for="inputComment">Add comment:</label>
        </div>
        <textarea class="form-control" id="newComment" rows="4"></textarea>
        <br />
        <button {{action 'addComment'}} class="btn btn-primary">Add comment</button>
      </div>
    </div>
  </div>
</script>


<script type="text/x-handlebars" data-template-name="bootstrapDropdown">
    
{{#view categorySelect tagName="div" class="btn-group dropdown" classBinding="isOpen:open"}}
  <a class="btn dropdown-toggle btn-small" data-toggle="dropdown" href="#">
    {{selected.title}}
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
    {{#each content}}
    <li {{bind-attr class="isActive:active"}}><a href="#" {{action "select" on="click"}}>{{title}}</a></li>
    {{/each}}
  </ul>
{{/view}}
    
</script>

</html>