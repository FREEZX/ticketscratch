<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends AUTH_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('users_model', 'users');
	}

	public function listing($from = 0, $perpage = 10){
		if(!$this->form_validation->is_natural($from) || ! $this->form_validation->is_natural($perpage)){
			show_404();
			return;
		}
		$data = array(
			'search' => $this->input->post('search')
		);
		$this->response($this->users->listing($data, $from, $perpage));
	}

	public function update($id = ''){
		$privs = $this->input->post('type');
		if($this->form_validation->is_natural_no_zero($id) && in_array($privs, array('AGENT', 'USER'))){
			if($this->users->update($id, $privs)){
				$this->response(array('result' => TRUE, 'data' => 'Успешно'));
			}else{
				$this->response(array('result' => FALSE, 'data' => 'Неуспешно'));
			}
		}else{
			$this->response(array('result' => FALSE, 'data' => 'Invalid ID or Privs data'));
		}
	}

	public function details($id){
		$this->response($this->users->details($id));
	}
}