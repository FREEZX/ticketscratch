<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Views extends CI_Controller {
	public function client(){
		$this->load->view('client_view');
	}

	public function agent(){
		$this->load->view('agent_view');
	}

	public function test(){
		$this->load->view('tabledepartment_view');
	}
}