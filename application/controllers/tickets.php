<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tickets extends AUTH_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('tickets_model','tickets');
	}

	public function listing($from=0, $perpage=20){
		if(!$this->form_validation->is_natural($from) || !$this->form_validation->is_natural_no_zero($perpage)){
			show_404();
			return;
		}
		$data = array(
			'search' => $this->input->post('search')
			);
		echo json_encode($this->tickets->listing($data, $from, $perpage, $this->input->get('categoty')));

	}

	public function insert(){
		$data = json_decode(file_get_contents('php://input'), TRUE);
		if($data===FALSE || isset($data['tickets'])) {
			show_404();
		}
		$data = $data['tickets'];
		if(isset($data['wizard'])){
			$wizard_data = $data['wizard'];	
		}else{
			$this->response(array('success' => FALSE));
		}
		if(isset($data['ticket'])){
			$ticket_data = $data['ticket'];
		}else{
			$this->response(array('success' => FALSE));
		}
		unset($data);
		if(count($wizard_data)>0){
			foreach($wizard_data AS $w){
				if(!$this->form_validation->is_natural_no_zero($w)){
					$this->response(array('success' => FALSE));
					exit();
				}
			}
		}else{
			$this->response(array('success' => FALSE));
			exit();
		}
		
		if(count($ticket_data)==0){
			$this->response(array('success' => FALSE));
			exit();
		}
		

		$this->response($this->tickets->insert($wizard_data, $ticket_data));
	}

	public function details($id = ''){
		if($this->form_validation->is_natural_no_zero($id)){
			$data = $this->tickets->getTicket($id);
			$this->response($data);
		}else{
			show_404();
		}

	}

	public function update($id = ''){
		if($this->form_validation->is_natural_no_zero($id)){
			$parsedData = json_decode(file_get_contents('php://input'), TRUE);
			if($parsedData===FALSE){
				show_404();
			}
			if($this->tickets->update($id, $parsedData)){
				$this->response(array('success' => TRUE));
			}else{
				$this->response(array('success' => FALSE, 'error' => "database problem"));
			}
		}else{
			show_404();
		}
	}

	public function ticket_change($id = ''){

			//$this->tickets->addComment($id, $this->input->post('comment'))){
	}
}