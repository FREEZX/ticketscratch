<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends AUTH_Controller {

	public function index() {
		$this->session->sess_destroy();	
		// $conf = $this->config->load('cas');
		// $this->load->library('cas');				//se povikuva bibliotekata za CAS
		// $this->cas->logout();						//se odlogorame od cas
		// redirect($conf['cas_server_url'].'/logout');	//oficijalno  odlogiranje od CAS
	}

}