<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questions extends AGENT_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('questions_model', 'questions');
	}

	public function listing(){
		$this->response(array('questions' => $this->questions->getAll()));
	}
	public function insert(){
		$data = json_decode(file_get_contents('php://input'), TRUE);
		if($data===FALSE){
			shoe_404();
		}
		$this->response(array('question_id' => $this->answers->add($data['question'])));
	}
}