<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Answers extends AGENT_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('answers_model', 'answers');
	}

	public function listing(){
		$this->response(array('answers' => $this->answers->getAll()));
	}

	public function insert(){
		$data = json_decode(file_get_contents('php://input'), TRUE);
		if($data===FALSE){
			show_404();
		}
		$id = $this->answers->add($data['answer']);
		$data['id'] = $id;
		$this->response(array('answer'=>$data));
	}
}
