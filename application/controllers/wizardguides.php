<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wizardguides extends AUTH_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('wizardsguide_model', 'wizardsguide');
	}

	public function listing($from=0, $perpage=0){
		exit();
	}

	public function details($id=''){
		$this->response($this->wizardsguide->getNext($id));
	}

	public function insert(){
		if($this->wizardsguide->add($this->input->post('ids'), $this->input->post('optional'))) {
			$this->response(array('success' => TRUE));
		}else{
			$this->response(array('success' => FALSE));
		}
	}

	public function update($ticketid = ''){
		$this->form_validation->set_rules('data', 'Answers', 'trim|required|is_json');
		if($this->form_validation->run()){
			if($this->wizardsguide->update($id, $this->input->post('data'))) {
				$this->response(TRUE);
			}else{
				$this->response(FALSE);
			}
		}else{
			$this->response(array('success' => FALSE, 'error' => 'validacija'));
		}
	}
}