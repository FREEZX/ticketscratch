<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wizards extends AUTH_NO_REST_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('wizards_model', 'wizards');
	}
	
	public function structure(){
		$this->$this->form_validation->set_rules('data', 'Structure Tree', 'trim|required');
		if($this->form_validation->run()){
			$data = json_decode($this->input->post('data'));
			if(isset($data['create'])){
				$this->createCommands($data['create']);
			}
			if(isset($data['update'])){
				$this->updateCommands($data['update']);
			}
			if(isset($data['delete'])){
				$this->createCommands($data['delete']);
			}
		}else{
			echo json_encode(array('success' => FALSE, 'error' => $this->form_validation->error_array()));
		}
	}

	private function deleteCommand($data){

	}

	
}