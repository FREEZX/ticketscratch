<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends AGENT_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('settings_model', 'settings');
	}
	//Data building
	public function details($key = ''){
		echo json_encode($this->settings->get($key));
	}

	public function insert(){
		$this->form_validation->set_rules('key', 'Key', 'trim|required');
		$this->form_validation->set_rules('value', 'Value', 'trim|required');
		if($this->form_vaalidation->run()){
			if($this->settings->add($this->input->post('key'), $this->input->post('value'))){
				$this->response(array('success' => TRUE));
			}else{
				$this->response(array('success' => FALSE));
			}
		}else{
			$this->response(array('success' => FALSE, 'error' => $this->form_validation->error_array()));
		}
	}

	public function update($id){
		$this->form_validation->set_rules('key', 'Key', 'trim|required');
		$this->form_validation->set_rules('value', 'Value', 'trim|required');
		if($this->form_vaalidation->run()){
			if($this->settings->update($id, $this->input->post('key'), $this->input->post('value'))){
				$this->response(array('success' => TRUE));
			}else{
				$this->response(array('success' => FALSE));
			}
		}else{
			$this->response(array('success' => FALSE, 'error' => $this->form_validation->error_array()));
		}
	}

}