<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departments extends AGENT_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('departments_model', 'departments');
	}


	//Actual functions
	public function insert(){

			$data = json_decode(file_get_contents('php://input'),TRUE);
			if($data===FALSE){
				$this->response(array('success' => FALSE));
			}else{
				$res = $this->departments->add($data['departments']);
				$this->response(array('success' => (bool) $res, 'data' => $res));
			}	
		

	}

	public function details($id='') {
		if(!$this->form_validation->is_natural_no_zero($id)){
			show_404();
		}
		else{
			$data = $this->departments->get($id);
			if($data){
				$result = array(
					'success' => TRUE,
					'data' => $data
				);
			}
			else{
				$result = array(
					'success' => FALSE
				);
			}
			$this->response($result);
		}
	}

	public function listing($from = 0, $perpage = 10) {
		if(!$this->form_validation->is_natural($from) || !$this->form_validation->is_natural_no_zero($perpage)){
			show_404();
			return;
		}
		$data = array(
			'search' => $this->input->post('search')
		);
		$this->response($this->departments->listing($data, $from, $perpage));
	}

	public function update($id='') {
		if($this->form_validation->is_natural_no_zero($id)){
			$data = json_decode(file_get_contents('php://input'), TRUE);
			if($data===FALSE){
				$this->response(array('success' => FALSE));
			}else{
				$res = $this->departments->update($data['departments']);
				$this->response(array('success' => (bool) $res, 'data' => $res));
			}
		}else{
			show_404();
		}

	}

	public function delete($id='') {
		if(!$this->form_validation->is_natural_no_zero($id)){
			show_404();
		}
		else{
			$this->response(array('success' => (bool) $this->departments->delete($id)));
		}
	}

}