<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	//TODO DELETE testing functions
	public function temp_login() {
		$this->load->model('users_model', 'users');
		$username = $this->input->post('username');

		if($res = $this->users->login($username)){
			$res['proxy'] = true;
			$this->session->set_userdata($res);
		}
		echo 'fail lol';
	}

	public function session_data() {
		var_dump($this->session->all_userdata());
	}
	
	public function index($location = '') {
		$this->load->library('cas');
		$this->cas->force_auth();
		$userData = $this->cas->user();
		$username = $userData->userlogin;

		if ($username===FALSE) {
			echo json_encode(FALSE);
			exit();
		}
		$this->load->model('users_model');
		$this->load->library('curl');
		$this->load->library('iknow');
		if($username[0]=='0')
		{
			$username = substr($username, 1);
		}
		$userId = $this->users_model->login($username);
		if ($userId===FALSE) {
			$info = $this->iknow->getStudentInfo($username);
			if(isset($info))
			{
				$uloga = $info->uloga;
				$uloga=preg_split("/;/ ", $uloga);
				if(is_array($uloga))
				{
					$uloga = $uloga[array_pop(array_keys($uloga))];
				}
				$userdata = array('username' => $username, 'firstname' => (strlen($info->Ime)>0 ? $info->Ime : 'Корисник'), 'lastname' => (strlen($info->Prezime)>0 ? $info->Prezime : 'Корисник'), 'user_type' => (strcmp($info->uloga, 'студенти')==0 ? 'USER' : 'MODERATOR'), 'email' => (is_numeric($username) ? $username.'@students.finki.ukim.mk' : $username.'@finki.ukim.mk'), 'role' => $uloga, 'NasokaId' =>$info->NasokaId);
				$userId = $this->users_model->registration($userdata);
			}
			else
			{
				echo json_encode(FALSE);
				exit();
			}
		}
		$userdata = $this->users_model->getUserData($userId);
		$userdata->loggedin = TRUE;

		if ($userdata) {
			$this->session->set_userdata($userdata);
		}
		$this->session->sess_expiration = '14400';
		$meM = intval(date("m", strtotime($this->session->userdata('created_on'))));
		$meY = intval(date("Y", strtotime($this->session->userdata('created_on'))));
		$curY = date('Y');
		$curSem = $this->iknow->getCurrentSemestry();

		if(in_array($meM, range(2,10)))
		{
			$sem = 2;
		}
		else
		{
			$sem=1;
		}
		if($curSem!=$sem || $curY!=$meY)
		{
			if($this->session->userdata('role')=='професори' || $this->session->userdata('role')=='асистенти')
			{
				$this->users_model->updateCourses(json_encode($this->iknow->getTeacherCourses($username)));
			}
			else
			{	
				$this->users_model->updateCourses(json_encode($this->iknow->getStudentCourses($username)));
			}
		}
		$url = base_url() . $location;
		redirect($url, 'refresh');
	}
}
?>