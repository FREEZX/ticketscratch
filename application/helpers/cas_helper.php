<?php
	function initCAS() {
		phpCAS::setDebug();
		phpCAS::client(CAS_VERSION_2_0, '194.149.138.7', 8443, '/cas');
		phpCAS::setNoCasServerValidation();
		phpCAS::forceAuthentication();
	}
?>
