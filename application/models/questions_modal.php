<?php
class Questions_Model extends CI_Model {

	public function getAll(){
		return $this->db->get('wizard_questions')
		->result_array();
	}

	public function add($question, $x, $y){
		$this->db->insert('wizard_questions', array('question' => $question, 'x' => $x, 'y' => $y));
		return $this->db->insert_id();
	}


	public function update($id, $question, $x, $y){
		$this->db->where('id', $id)
		->update('wizard_questions', array('question' => $question, 'x' => $x, 'y' => $y));
		return TRUE;
	}


}