<?php
class Wizardsguide_Model extends CI_Model {


	public function add($ids, $opt){
		$data = array();
		$this->db->trans_start();
		$this->db->insert('tickets', array('user_id' => $this->session->userdata('id'), 'info' => $opt));
		$id = $this->db->insert_id();
		for($i=0;$i<count($ids); $i++){
			//$data[] = array('rel_wizard_questions_answers_id' => $k, 'ticket_id' => $id);
			$this->db->insert('rel_tickets_wizard', array('rel_wizard_questions_answers_id' => $ids[$i], 'ticket_id' => $id));
		}
		//$this->db->insert('rel_tickets_wizard', $data);
		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE){
			//echo 'test';
			return FALSE;
		}
		//echo 'test';
		return TRUE;

	}

	public function getNext($id){
		$data = array();
		$quest = $this->db->select('wizard_questions.question, wizard_questions_tree.question_id', FALSE)
		->from('wizard_questions_tree')
		->join('wizard_questions', 'wizard_questions_tree.question_id=wizard_questions.id', 'inner')
		->where('wizard_questions_tree.question_answer_id', $id)
		->limit(1)
		->get();

		if($quest!==FALSE && $quest->num_rows()>0){
			$data['question'] = $quest->row()->question;
			$data['answers'] = $this->db->select('wizard_answers.answer AS answer_text, rel_wizard_questions_answers.id AS answer_id',FALSE)
			->from('rel_wizard_questions_answers')
			->join('wizard_answers', 'rel_wizard_questions_answers.answer_id=wizard_answers.id', 'inner')
			->where('rel_wizard_questions_answers.question_id', $quest->row()->question_id)
			->get()
			->result_array();
			return $data;
		}else{
			return FALSE;
		}

	}


}