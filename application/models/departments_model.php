<?php
class Departments_Model extends CI_Model {
	function add($data) {
		$this->db->insert('departments', $data);
		return $this->db->insert_id();
	}

	function get($id){
		$q = $this->db->where('id', $id)->get('departments', 1);
		if($q->num_rows() !== 0){
			return $q->row_array();
		}
		return FALSE;
	}

	function listing($data, $from, $perpage){
		$result = array();
		if($data['search']){
			$q = $this->db->like('name', $data['search'])
			->or_like('description', $data['search'])
			->get('departments', $perpage, $from);

			if($q!==FALSE && $q->num_rows()>0){
				$result['departments'] = $q->result_array();
			}
			else{
				$result['departments'] = array();
			}

			$result['meta'] = array('total' => $this->db->like('name', $data['search'])
				->or_like('description', $data['search'])
				->count_all('departments'));
		}else{
			$q = $this->db->get('departments', $perpage, $from);

			if($q!==FALSE && $q->num_rows()>0){
				$result['departments'] = $q->result_array();
			}
			else{
				$result['departments'] = array();
			}

			$result['meta'] = array('total' => $this->db->count_all('departments'));
		}

		$result['users'] = $this->db->select('DISTINCT users.*', FALSE)
		->from('users')
		->join('departments', 'users.id=departments.accountable_user OR users.id=departments.manager', 'inner', FALSE)
		->get()
		->result_array();
		return $result;
	}

	function update($id, $data){
		$q = $this->db->where('id', $id)->update('departments', $data);
		return $q;
	}

	function delete($id){
		$q = $this->db->where('id', $id)->delete('departments');
	}
}
