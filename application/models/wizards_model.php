<?php
class Wizards_Model extends CI_Model {

	public function getAll($data, $from, $perpage){
		return $this->db->get('wizard_questions')
		->result_array();
	}

	public function deleteQuestion($id){
		$this->db->where('id', $id)
		->delete('wizard_questions');

		return TRUE;
	}

	public function deleteAnswer($id){
		$this->db->where('id', $id)
		->delete('wizard_answers');

		return TRUE;
	}

	public function deleteRelation($id){
		$this->db->where('id', $id)
		->delete('rel_wizard_questions_answers');

		return TRUE;
	}

	public function updateQuestion($id, $text){
		$this->db->where('id', $id)
		->update('wizard_questions', array('text' => $text));

		return TRUE;
	}

	public function updateAnswer($id, $text){
		$this->db->where('id', $id)
		->update('wizard_answers', array('text' => $text));

		return TRUE;
	}

	public function updateRelation($id, $qid, $aid){
		$this->db->where('id', $id)
		->update('rel_wizard_questions_answers', array('question_id' => $qid, 'answer_id' => $aid));

		return TRUE;
	}

	public function insertQuestion($text){
		$this->db->insert('wizard_questions', array('text' => $text));

		return TRUE;
	}

	public function insertAnswer($text){
		$this->db->insert('wizard_answers', array('text' => $text));

		return TRUE;
	}

	public function insertRelation($qid, $aid){
		$this->db->insert('rel_wizard_questions_answers', array('question_id' => $qid, 'answer_id' => $aid));

		return TRUE;
	}

	public function getQuestion($id){
		$q = $this->db->where('id', $id)
		->get('wizard_questions', 1);

		if($q!==FALSE && $q->num_rows()>0){
			return $q->row_array();
		}
		return FALSE;
	}

	public function getAnswersByQuestionID($id){
		$q = $this->db->select('rel_wizard_questions_answers.id AS id, wizard_answers.text AS text')
		->from('rel_wizard_questions_answers')
		->join('wizard_answers', 'rel_wizard_questions_answers.answer_id=wizard_answers.id', 'INNER')
		->where('rel_wizard_questions_answers.question_id', $id);

		if($q!==FALSE && $q->num_rows()>0){
			return $q->row_array();
		}
		return FALSE;

	}


}