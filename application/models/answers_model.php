<?php
class Answers_Model extends CI_Model {

	public function getAll(){
		return $this->db->get('wizard_answers')
		->result_array();
	}

	public function add($data){
		$this->db->insert('wizard_answers', array('answer' => $data));
		return $this->db->insert_id();
	}
}