<?php
class Tickets_Model extends CI_Model {

	public function listing($data, $from, $perpage, $category=''){

		$result = array();
		if($this->session->userdata('proxy')=='PROXY'){
			if($data['search']){
				$q = $this->db->like('name', $data['search'])
				->or_like('description', $data['search'])
				->order_by('time_updated', 'DESC')
				->get_where('tickets', array('assigned_department_id' => NULL), $perpage, $from);


				if($q!==FALSE && $q->num_rows()>0){
					$result['tickets'] = $q->result_array();
				}
				else{
					$result['tickets'] = null;
				}

				$result['meta'] = array('total' => $this->db->like('name', $data['search'])
					->or_like('description', $data['search'])
					->where('tickets', array('assigned_department_id' => NULL))
					->count_all_results());
			}else{
				
				$q = $this->db->order_by('time_updated', 'DESC')
				->get_where('tickets',array('assigned_department_id' => NULL), $perpage, $from);

				if($q!==FALSE && $q->num_rows()>0){
					$result['tickets'] = $q->result_array();
				}
				else{
					$result['tickets'] = null;
				}

				$result['meta'] = array('total' => $this->db->where('assigned_department_id', NULL)
					->from('tickets')
					->count_all_results());
			}
		}else if($this->session->userdata('user_type')=='AGENT'){
			if($data['search']){
				$q = $this->db->like('tickets.name', $data['search'])
				->or_like('tickets.description', $data['search'])
				->from('tickets')
				->join('departments', 'departments.id=tickets.assigned_department_id', 'inner', 'inner',FALSE)
				->join('rel_users_departments', 'rel_users_departments.department_id=departments.id','inner',FALSE)
				->order_by('tickets.time_updated', 'DESC')
				->where('rel_users_departments.user_id', $this->session->userdata('id'));


				if($category=='my_tickets'){
					$this->db->where('tickets.assigned_user_id', $this->db->session->userdata('id'));
				}else if($category=='open'){
					$this->db->where('tickets.status <> "CLOSE"',FALSE);
				}else if($category=='closed'){
					$this->db->where('tickets.status', 'CLOSED');
				}else if($category=='reopened'){
					$this->db->where('tickets.status', 'REOPENED');
				}else if($category=='info_needed'){
					$this->db->where('tickets.status', 'INFO_NEEDED');
				}


				$this->db->limit($perpage, $from)
				->get();
				$result['tickets'] = $q->result_array();
				$q = null;

				$q = $this->db->like('tickets.name', $data['search'])
				->or_like('tickets.description', $data['search'])
				->from('tickets')
				->join('departments', 'departments.id=tickets.assigned_department_id', 'inner', 'inner',FALSE)
				->join('rel_users_departments', 'rel_users_departments.department_id=departments.id','inner',FALSE)
				->order_by('tickets.time_updated', 'DESC')
				->where('rel_users_departments.user_id', $this->session->userdata('id'));
				if($category=='my_tickets'){
					$this->db->where('tickets.assigned_user_id', $this->db->session->userdata('id'));
				}else if($category=='open'){
					$this->db->where('tickets.status <> "CLOSE"',FALSE);
				}else if($category=='closed'){
					$this->db->where('tickets.status', 'CLOSED');
				}else if($category=='reopened'){
					$this->db->where('tickets.status', 'REOPENED');
				}else if($category=='info_needed'){
					$this->db->where('tickets.status', 'INFO_NEEDED');
				}

				$result['meta'] = array('total' => $this->db->count_all_results());
			}else{
				$q = $this->db->select('tickets.*', FALSE)
				->from('tickets')
				->join('departments', 'departments.id=tickets.assigned_department_id', 'inner', FALSE)
				->join('rel_users_departments', 'rel_users_departments.department_id=departments.id','inner', FALSE)
				->where('rel_users_departments.user_id', $this->session->userdata('id'));
				if($category=='my_tickets'){
					$q = $this->db->where('tickets.assigned_user_id', $this->db->session->userdata('id'));
				}else if($category=='open'){
					$q = $this->db->where('tickets.status <> "CLOSE"',FALSE);
				}else if($category=='closed'){
					$q = $this->db->where('tickets.status', 'CLOSED');
				}else if($category=='reopened'){
					$q = $this->db->where('tickets.status', 'REOPENED');
				}else if($category=='info_needed'){
					$q = $this->db->where('tickets.status', 'INFO_NEEDED');
				}

				$q = $this->db->limit($perpage, $from)
				->get();
				$result['tickets'] = $q->result_array();

				$q = $this->db->join('departments', 'departments.id=tickets.assigned_department_id', 'inner', FALSE)
				->join('rel_users_departments', 'rel_users_departments.department_id=departments.id','inner', FALSE)
				->where('rel_users_departments.user_id', $this->session->userdata('id'));
				if($category=='my_tickets'){
					$this->db->where('tickets.assigned_user_id', $this->db->session->userdata('id'));
				}else if($category=='open'){
					$this->db->where('tickets.status <> "CLOSE"',FALSE);
				}else if($category=='closed'){
					$this->db->where('tickets.status', 'CLOSED');
				}else if($category=='reopened'){
					$this->db->where('tickets.status', 'REOPENED');
				}else if($category=='info_needed'){
					$this->db->where('tickets.status', 'INFO_NEEDED');
				}


				$result['meta'] = array('total' => $this->db->from('tickets')->count_all_results());
			}
		}else{
			if($data['search']){
				$q = $this->db->like('name', $data['search'])
				->or_like('description', $data['search'])
				->order_by('time_updated', 'DESC')
				->get_where('tickets',array('user_id'=>$this->session->userdata('id')), $perpage, $from);

				$result['tickets'] = $q->result_array();

				$result['meta'] = array('total' => $this->db->like('name', $data['search'])
					->or_like('description', $data['search'])
					->where('user_id', $this->session->userdata('id'))
					->from('tickets')
					->count_all_results());
			}else{
				$q = $this->db->order_by('time_updated', 'DESC')
				->get_where('tickets', array('user_id'=>$this->session->userdata('id')), $perpage, $from);
				$result['tickets'] = $q->result_array();


				$result['meta'] = array('total' => $this->db->where('user_id', $this->session->userdata('id'))
					->from('tickets')
					->count_all_results());
			}
		}
		return $result;
	}

	public function add($wizard_data, $ticket_data){

		$tdata = array(
			'status' => 'OPEN',
			'user_id' => $this->session->userdata('id')!==FALSE ? $this->session->userdata('id') : NULL,
			'user_email' => $this->session->userdata('email'),
			'assigned_department_id' => NULL,
			'assigned_user_id' => NULL,
			'info' => $ticket_data,
			'priority' => 'MEDIUM');

		$this->db->trans_start(TRUE); //auto-rollback with TRUE
		$this->db->insert('tickets', $tdata);
		$ticketID = $this->db->insert_id();

		foreach($wizard_data AS $k){
			$zdata = array(
				'rel_wizard_questions_answers_id' => $k['rwqaid'],
				'ticket_id' => $ticketID
				);

			$this->db->insert('rel_tickets_wizard', $zdata);
			unset($zdata);
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}else{
			return TRUE;
		}
	}

	public function update($id, $data){
		$q = $this->db->where('id', $id)
		->update('tickets',$data);
		if($q){
			return TRUE;
		}
		return FALSE;
	}

	public function getTicket($id){
		$data['ticket'] = $this->db->get_where('tickets',array('id' => $id), 1)->row_array();
		$timeline = $this->db->order_by('time_change', 'DESC')
		->get_where('tickets_timeline', array('ticket_id' => $id))
		->result_array();

		$data['ticket']['timeline'] = array();
		foreach ($timeline as $key => $value) {
			$data['ticket']['timeline'][] = $value['id'];
		}

		$data['timeline'] = $timeline;

		return $data;
	}
}