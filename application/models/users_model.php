<?php
class Users_Model extends CI_Model {

	public function login($username) {
		$q = $this->db->get_where('users', array('username' => $username), 1);

		if($q->num_rows() == 1){
			return $q->row_array();
		}
		return FALSE;
	}

	public function listing($data, $from, $perpage){
		$result = array();
		if($data['search']){
			$q = $this->db->like('username', $data['search'])
			->or_like("CONCAT(firstname,' ',lastname)", $data['search'])
			->get('users', $perpage, $from);

			if($q!==FALSE && $q->num_rows()>0){
				$result['users'] = $q->result_array();
			}else{
				$result['users'];
			}

			$result['meta'] = array('total' => $this->db->like('username', $data['search'])
				->or_like("CONCAT(firstname,' ',lastname)", $data['search'])
				->count_all('users'));

		}else{
			$q = $this->db->get('users', $perpage, $from);

			if($q!==FALSE && $q->num_rows()>0){
				$result['users'] = $q->result_array();
			}else{
				$result['users'];
			}

			$result['meta'] = array('total' => $this->db->count_all('users'));
		}
		return $result;
	}

	public function update($id, $privs){
		$q = $this->db->where('id', $id)
		->update('users', array('user_type' => $privs));

		if($q){
			return TRUE;
		}
		return FALSE;
	}

	public function details($id){
		return array('users' => $this->db->get_where('users', array('id' => $id), 1)
		->row_array());
	}
}
