<?php
class Settings_Model extends CI_Model {

	public function get($key){
		return $this->db->get_where('settings', array('key' => $key), 1)
		->row_array();
	}

	public function insert($key, $value){
		$this->db->insert('settings',array('key' => $key, 'value' => $value));

		return TRUE;
	}

	public function update($id){
		$this->db->where('id', $id)
		->update('settings',array('key' => $key, 'value' => $value));

		return TRUE;
	}

}