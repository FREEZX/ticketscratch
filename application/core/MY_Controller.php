<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'libraries/REST_Controller.php');
class AUTH_Controller extends REST_Controller {
	public function __construct(){
		parent::__construct();
		$this->form_validation->set_error_delimiters('', '');
		if($this->session->userdata('id')===FALSE){
			header('Location: '.base_url().'index.php/login', 'refresh');
		}
	}

	public function _remap($method)
	{
		if(parent::isGetRequest()){
			if($this->form_validation->is_natural_no_zero($this->input->get('limit')))
			{
				if($this->form_validation->is_natural_no_zero($this->input->get('skip'))){
					$this->listing($this->input->get('skip'), $this->input->get('limit'));
				}else{
					$this->listing(0, $this->input->get('limit'));
				}
			}else{
				if($this->form_validation->required($this->uri->segment(2))){
					$this->details($this->uri->segment(2));
				}else{
					show_404();
				}
			}
		}
		elseif(parent::isPostRequest()){
			$this->insert();
		}
		elseif(parent::isPutRequest()){
			$this->update($this->uri->segment(2));
		}
		elseif(parent::isDeleteRequest()){
			$this->delete($this->uri->segment(2));
		}
	}
}

class AGENT_Controller extends AUTH_Controller {
	public function __construct(){
		parent::__construct();
		if($this->session->userdata('user_type')!='AGENT'){
			show_404();
		}
	}
}

class AUTH_NO_REST_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		if($this->session->userdata('user_type')!='AGENT'){
			show_404();
		}
	}
}