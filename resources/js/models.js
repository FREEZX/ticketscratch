$(function(){
	App.Ticket = DS.Model.extend({
		info: DS.attr('string'),
		priority: DS.attr('string'),
		status: DS.attr('string'),
		time_created: DS.attr('date'),
		time_updated: DS.attr('date'),
		user_email: DS.attr('string'),
		assigned_department_id: DS.belongsTo('department'),
		assigned_user_id: DS.belongsTo('user'),
		user_id: DS.belongsTo('user'),
		timeline: DS.hasMany('timeline')
	});

	App.Timeline = DS.Model.extend({
		ticket_id: DS.belongsTo('ticket'),
		change_type: DS.attr('string'),
		change_data: DS.attr('string'),
		change_user: DS.belongsTo('user'),
		time_change: DS.attr('date'),
	});


	App.User = DS.Model.extend({
		username: DS.attr('string'),
		firstname: DS.attr('string'),
		lastname: DS.attr('string'),
		user_type: DS.attr('string'),
		email: DS.attr('string'),
		fullname: function(){
			return this.get('firstname') + ' ' + this.get('lastname');
		}.property('firstname', 'lastname')
	});

	App.Department = DS.Model.extend({
		name: DS.attr('string'),
		description: DS.attr('string'),
		manager: DS.belongsTo('user'),
		accountable_user: DS.belongsTo('user')
	});

	App.Setting = DS.Model.extend({
		key: DS.attr('string'),
		value: DS.attr('string'),
	});

	App.Answer = DS.Model.extend({
		answer: DS.attr('string')
	});

	App.Question = DS.Model.extend({
		question: DS.attr('string'),
		x: DS.attr('number'),
		y: DS.attr('number')
	});
});