// Dropdown = Ember.View.extend({
//   open: false,
//   defaultTemplate: Ember.computed(function() {
//     return Ember.Handlebars.compile('\
//     <div class="btn-group customddbtn" {{bindAttr class="open"}}>\
//       <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">\
//       	Status\
//       	<span class="caret"></span>\
//       	<span class="sr-only">Toggle Dropdown</span>\
//       </button>\
//       <ul class="dropdown-menu" role="menu">\
//         <li><a href="#">Test</a></li>\
//         <li><a href="#">Test 2</a></li>\
//       </ul>\
//     </div>\
//   ');
//   }),
//   click: function(e) {
//     if (this.get('open')) {
//       this.set('open', false);
//     } else {
//       this.set('open', true);
//     }
//     e.stopPropagation();
//   },
//   didInsertElement: function() {
//     var view;
//     view = this;
//     $('body').on('click', function() {
//       view.set('open', false);
//     });
//   }
// });


$(function(){
	App.SearchBox = Ember.TextField.extend({
		classNames: ['search_box'],
		keyUp: function(event){
			var query = $(event.target).val();
			if (event.keyCode == 27){
				this.$().siblings('.search-result').addClass('hidden');
				this.set('value',"");
				$(this).trigger('focusOut');
			}else if( event.keyCode == 40 ){
				App.SearchResultNavigateDown(this.$().siblings("table"));
			}else if( event.keyCode == 38 ){
				App.SearchResultNavigateUp(this.$().siblings("table"));
			}else if( event.keyCode == 13 ){
				App.SearchResultNavigateIn(this.$().siblings("table"));
				this.$().trigger("focusOut");
			}else if (query.length > 0){
				this.search(query);
				this.$().siblings('.search-result').removeClass('hidden');
			}else if (query.length === 0){
				this.$().siblings('.search-result').addClass('hidden');
			}
		},
		focusOut:function(){
			this.$().css('border-color','#dce4ec');
		},
		click: function(){
			this.$().trigger("keyUp");
			this.$().siblings('.search-result').addClass('hidden');
		},
		search: function(query) {
	    	//implement in children
		},
		searchResultSelected: function() {
			this.set('value','');
		},
		change:function(event) {
			event.stopPropagation();
			event.preventDefault();
		}
	});

	App.AgentSearchBox = App.SearchBox.extend({
		placeholder: 'Type to locate the organisation, branch or specific location',
		search: function(query) {
			this.get('controller').search_agents(query);
		}
	});

	App.AssetSearchBox = App.SearchBox.extend({
		maxResults: 5,
		placeholder: 'What is the primary use for the property?',
		search: function (query) {
			var results = this.get('parentView').get('asset_search_results'),
			scope = this;
			results.set('content', []);
			if (query === '') return;
			var query_params = query.split(" ");
			for (var asset_index = 0; asset_index < App.Assets.length; asset_index++) {
				var asset = App.Assets[asset_index];
				if (scope.match(query_params, asset)) {
					results.addObject(asset);
				}
				if (results.get('content').length >= scope.maxResults) break;
			}
			if (results.get('content').length > 0) this.get('parentView').$().find('.asset_search_result').removeClass('hidden');
		},

		match: function (query_params, asset_hash) {
			var matches = true;
			query_params.forEach(function (query_param) {
				var values = $.map(asset_hash, function (value) {
					return value;
				});
				var regex = new RegExp(query_param, "i");
				matches = matches && (values.join(" ").match(regex) != null);
			});
			return matches;
		}
	});

});

Array.prototype.last = function() {
	return this[this.length-1];
}

Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
	if (arguments.length < 3)
		throw new Error("Handlebars Helper equal needs 2 parameters");
	if( lvalue!==rvalue ) {
		return options.inverse(this);
	} else {
		return options.fn(this);
	}
});