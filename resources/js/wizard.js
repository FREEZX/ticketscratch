$(document).ready(function(){
	$('#wizardmodal').modal({
		show: false
	})
	var wizardcache = [];
	var wizardanswers = [];
	wizardanswers['ids'] = [];
	wizardanswers['description'] = '';
	$(document).on('click', '.createwithemail', function(){
		$.get(base_url+'index.php/wizardguides/1', function(data){
			wizardcache.push(data);
			data.hasback = false;
			var source   = $('#wizardguidetemplate').html();
			var template = Handlebars.compile(source);
			var html    = template(data);
			$('#wizardmodal').html(html);
			$('#wizardmodal').modal('show');
		})
	}),

	$(document).on('click', '.wizardnext', function(){
		var answer = $('.modal').find('input[name=answer]').filter(':checked').val();
		if(answer!==undefined){
			wizardanswers['ids'].push(answer);
			$.get(base_url+'index.php/wizardguides/'+answer, function(data){
				if(data){
					wizardcache.push(data);
					data.lastanswer = '';
					data.hasback = true;
					var source   = $("#wizardguidetemplate").html();
					var template = Handlebars.compile(source);
					var html    = template(data);
					$('#wizardmodal').html(html);
					$('#wizardmodal').modal('show');
				}else{
					wizardcache.push({});
					var source   = $("#wizardguidefinishtemplate").html();
					var template = Handlebars.compile(source);
					$('#wizardmodal').html(template());
					$('#wizardmodal').modal('show');
				}
			});
		}
	}),

	$(document).on('click', '.wizardback', function(){
		if(wizardcache.length > 1)
		{
			hasback = true;
		}else{
			hasback = false;
		}
		wizardcache.pop();
		var lastdata = wizardcache.last();
		lastdata.lastanswer = wizardanswers['ids'].pop();
		lastdata.hasback = hasback;
		var source   = $("#wizardguidetemplate").html();
		var template = Handlebars.compile(source);
		var html    = template(lastdata);
		$('#wizardmodal').html(html);
		$('#wizardmodal').modal('show');
	})

	$(document).on('click', '.wizardfinish', function(){
		console.log(wizardanswers.ids);
		$.post(base_url+'index.php/wizardguides/', {ids: wizardanswers.ids, optional: $('#description').val()}, function(data){
				$('#wizardmodal').modal('hide');
			// var obj = JSON.parse(data);
			// if(obj.success=='TRUE'){
			// }
		});
	})

});