$(function(){

	$(window).on('resize', function(){
		var textArea = $('.wizard-textarea');
		var width = textArea.parent().width();
		textArea.css({'max-width': width, 'min-width': width});
	});

	App = Em.Application.create();

	var currentPage = 1;
	var perPage = 20;

	App.Store = DS.Store.extend({
		revision: 1,
		
		adapter: DS.RESTAdapter.extend({
			url: "http://127.0.0.1/",
			namespace: 'ticket/index.php'
		})
	});

	App.Router.map(function(){
		this.resource('tickets', function(){
			this.resource('category', {path: '/:category'});
		});
		this.resource('ticket', {path: '/ticket/:ticketid'});
		this.resource('options', function(){
			this.resource('departments');
			this.resource('users');
			this.resource('system');
			this.resource('wizard');
		});
	});

	//Departments
	App.DepartmentsRoute = Em.Route.extend({
		model: function(){
			return this.store.find('department', {limit: 300000});
		}
	});

	App.DepartmentsController = Em.Controller.extend({
		deleteItem : null,
		selectedManager: null,
		selectedAccountable: null,
		newName: null,
		newDescription: null,

		agents: function(){
			return this.store.find('user', {limit: 300000, type: 'agent'});
		}.property('agents'),

		actions: {
			startDelete: function(item){
				this.set('deleteItem', item);
			},
			deleteDepartment: function(item){
				item.deleteRecord();
				item.save();
			},
			addDepartment: function(){
				var department = this.store.createRecord('department', {
					name: this.get('newName'),
					description: this.get('newDescription'),
					manager: this.get('selectedManager'),
					accountable_user: this.get('selectedAccountable')
				});
				department.save();
			}
		}
	});

	//Users
	App.UsersRoute = Em.Route.extend({
		model: function(){
			return this.store.find('user', {limit: 300000});
		}
	});

	App.UsersController = Em.Controller.extend({
		userTypes: [
			{"content": "User", user_type: "USER"},
			{"content": "Agent", user_type: "AGENT"}
		],
		actions: {
			changeType: function(userval){
				userval.user.set('user_type', userval.value);
				userval.user.save();
			}
		}
	});

	App.ClientTypeSelectView = Ember.Select.extend({
		change: function () {
			this.get('controller').send('changeType', {user: this.user, value: this.get('value')});
		}
	});

	//System
	App.SystemRoute = Em.Route.extend({
		model: function(){
			var model = Em.Object.create({
				'users': this.store.find('user', {limit: 300000, type: 'agent'})
			});

			return model;
		},
	});

	App.SystemController = Em.ObjectController.extend({
		selectedManager: null,
		selectedProxy: null,
		actions: {
			save: function(){
				
			}
		}
	});

	//Wizard
	App.WizardRoute = Em.Route.extend({
		model: function(){
			var model = Em.Object.create({
				answers: this.store.find('answer', {limit: 300000}),
				questions: this.store.find('question', {limit: 300000})
			});
			return model;
		},
		renderTemplate: function(){
			this.render();
			this.render('wizardSidebar', {
				outlet: 'sidebar'
			});
		}
	});

	App.WizardView = Em.View.extend({

		didInsertElement: function() {
			// This will run for every child view. But I want code to run only once when all children and root block are rendered
			Ember.run.scheduleOnce('afterRender', this, function(){
				initJsplumb();
				$(window).trigger('resize');
				initEndpoints();
			});
		}
	});

	App.WizardController = Em.Controller.extend({
		actions: {
			addWizardNode: function(){
				var contentElem = $('#newNodeContent');
				var parentElem = $('#flowchart-demo');
				addNode(contentElem.val(), parentElem);
			},
			chooseAnswer: function(answer){
				$('#answersList li').removeClass('active');
				var li = $('li[data-id='+answer.id+']');
				li.addClass('active');
				selectedQuestionText = answer.get('answer');
			},
			addAnswer: function(){
				var answer = this.store.createRecord('answer', {answer: $('#newAnswer').val()});
				answer.save();
			},
			saveWizard: function(){
				var nodes = [];

				$('.window').each(function(){
					nodes.push({id: this.id, question: $(this).text()});
				});


				console.log(nodes);

				var rawConnections = plumbInstance.getConnections();

				var connections = [];
				$.each(rawConnections, function(){
					connections.push({sourceId: this.sourceId, targetId: this.targetId, answer: this.getOverlay("label").getLabel()});
				});
				console.log(connections);

				var obj = {
					questions: nodes,
					answers: connections
				};

				//TODO add save query

			}
		}
	});


	App.IndexRoute = Em.Route.extend({
		beforeModel: function() {
			this.transitionTo('category', 'open');
		}
	});

	App.TicketsRoute = Em.Route.extend({
		beforeModel: function() {
			this.transitionTo('category', 'open');
		}
	});

	//Tickets Details
	App.TicketRoute = Em.Route.extend({
		model: function(params){
			return this.store.find('ticket', params.ticketid);
		},
		setupController: function(controller, model){
			controller.set('model', model);
			controller.set('departments', this.store.find('department', {limit: 300000}));
			// model.reload();
		}
	});

	App.TicketController = Em.ObjectController.extend({
		priorities: [
			{"content": "Low", value: "LOW"},
			{"content": "Medium", value: "MEDIUM"},
			{"content": "High", value: "HIGH"}
		],
		statuses: [
			{"content": "Open", value: "OPEN"},
			{"content": "Closed", value: "CLOSED"}
		],

		actions: {
			addComment: function(){
				var content = $('#newComment').val();
				var comment = this.get('store').createRecord('timeline', {
					ticket_id: this.get('model'),
					change_type: 'COMMENT',
					change_data: content
				});
				comment.save().then(function(){
					// ticket.addObject();
				});
			},
			changePriority: function(data){
				this.get('model').set('priority', data.value);
				this.get('model').save();
			},
			changeDepartment: function(data){
				this.get('model').set('department', data.value);
				this.get('model').save();
			},
			changeStatus: function(data){
				this.get('model').set('status', data.value);
				this.get('model').save();
			}
		}
	});

	App.StatusSelectView = Ember.Select.extend({
		change: function () {
			this.get('controller').send('changeStatus', {value: this.get('value')});
		}
	});

	App.PrioritySelectView = Ember.Select.extend({
		change: function () {
			this.get('controller').send('changePriority', {value: this.get('value')});
		}
	});

	App.DepartmentSelectView = Ember.Select.extend({
		change: function () {
			this.get('controller').send('changeDepartment', {value: this.get('value')});
		}
	});


	//Categories

	App.CategoryController = Em.ArrayController.extend({
		page: 1,
		perPage: 30,

		getMore: function(){
			if (this.get('loadingMore')) return;
				this.set('loadingMore', true);

			this.get('target').send('getMore');
		},

		// Also add a method `gotMore` that the route can call back to
		// notify the controller that the new data is in and it can stop
		// showing its loading indicator
		gotMore: function(items, page) {
			this.set('page', page);
			var controller = this;
			items.then(function(data){
				controller.set('content', controller.store.all('ticket'));
				controller.set('loadingMore', false);
				controller.refreshHasMore();
			});
		},

		refreshHasMore: function() {
			if(this.store.metadataFor("ticket").total <= this.page*this.perPage ){
				this.set('hasMore', false);
			}
			else{
				this.set('hasMore', true);
			}
		}
	});

	App.CategoryRoute = Em.Route.extend({
		model: function(params){
			this.set('category', params.category);
			return this.store.find('ticket', {limit: 30, category: params.category});
		},

		setupController: function(controller, data) {
			controller.set('model', data);
			this.get('controller').refreshHasMore();
		},

		actions: {
			getMore: function() {
				var controller = this.get('controller'),
				nextPage = controller.get('page') + 1,
				perPage = controller.get('perPage'),
				items;

				items = this.fetchPage(nextPage, perPage);
				// alert the controller to the new data
				controller.gotMore(items, nextPage);
			},
			rerender: function(){
				this.render();
			},
			openDetails: function(){

			}
		},
		// load another page's worth of tickets
		fetchPage: function(page, perPage) {
			var controller = this.get('controller');
			var args = {
				limit: controller.perPage, 
				skip: (controller.page++) * controller.perPage, 
				category: this.get('category')
			};
			return this.store.find('ticket', args);
		}
	});


	//Handlebars helpers
	Em.Handlebars.registerBoundHelper('date', function(value, options){
		return new moment(value).fromNow();
	});
});