window.addNode = null;
window.initEndpoints = null;
var plumbInstance = null;
function initJsplumb(){

	plumbInstance = jsPlumb.getInstance({
		// default drag options
		DragOptions : { cursor: 'pointer', zIndex: 2000 },
		// the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
		// case it returns the 'labelText' member that we set on each connection in the 'init' method below.
		ConnectionOverlays : [
			[ "Arrow", { location:1 } ],
			[ "Label", { 
				location:0.6,
				id:"label",
				cssClass:"aLabel"
			}]
		],
		Container:"flowchart-demo"
	});		

	// this is the paint style for the connecting lines..
	var connectorPaintStyle = {
		lineWidth:3,
		strokeStyle:"#5bc0de",
		joinstyle:"round",
		outlineColor:"white",
		outlineWidth:2
	},
	// .. and this is the hover style. 
	connectorHoverStyle = {
		lineWidth:3,
		outlineWidth:2,
		outlineColor:"white",
		strokeStyle:"#3498db"
	},
	targetEndpointHoverStyle = {
		fillStyle:"#3498db"
	},
	sourceEndpointHoverStyle = {
		strokeStyle:"#3498db"
	},
	// the definition of source endpoints (the small blue ones)
	sourceEndpoint = {
		endpoint:"Dot",
		anchor:[ "BottomCenter", { shape:"Square", anchorCount:150 }],
		paintStyle:{ 
			strokeStyle:"#5bc0de",
			fillStyle:"transparent",
			radius:7,
			lineWidth:3
		},
		isSource:true,
		maxConnections: 10,
		connector:[ "Flowchart", { stub:[30, 30], gap:8, cornerRadius:10, alwaysRespectStubs:true }],
		connectorStyle:connectorPaintStyle,
		hoverPaintStyle:sourceEndpointHoverStyle,
		connectorHoverStyle:connectorHoverStyle,
        dragOptions:{},
        overlays:[
			[ "Label", {
			location:[0.5, 1.5],
			label:"Drag",
			cssClass:"endpointSourceLabel" 
			} 
			]
		]
	},
	// the definition of target endpoints (will appear when the user drags a connection) 
	targetEndpoint = {
		endpoint:"Dot",
		paintStyle:{ fillStyle:"#5bc0de",radius: 8 },
		hoverPaintStyle:targetEndpointHoverStyle,
		maxConnections:-1,
		dropOptions:{ hoverClass:"hover", activeClass:"active" },
		isTarget:true,
        overlays:[
			[ "Label", { location:[0.5, -0.5], label:"Drop", cssClass:"endpointTargetLabel" } ]
        ]
	},

	init = function(connection) {
		if(typeof(selectedQuestionText) !== "undefined"){

			//Loop check
			var passed = [connection.sourceId];
			
			nextList = [connection.targetId];
			var loop = false;

			while(nextList.length > 0){
				var currentNode = nextList.shift();
				passed.push(currentNode);
				var connections = plumbInstance.getConnections({source: currentNode});
				for(var i=0; i<connections.length; ++i){
					if(_.contains(passed, connections[i].targetId)){
						loop = true;
						break;
					}
					nextList.push(connections[i].targetId);
				}
				if(loop){
					break;
				}
			}

			if(loop){
				jsPlumb.detach(connection); 
				alert("Loops are not allowed!");
			}
			else{
				connection.getOverlay("label").setLabel(
					selectedQuestionText
				);
				connection.bind("editCompleted", function(o) {
					if (typeof console != "undefined")
						console.log("connection edited. path is now ", o.path);
				});
			}

		}
		else{
			jsPlumb.detach(connection); 
			alert("Please select an answer first!");
		}
	};

	var _addEndpoints = function(toId, sourceAnchors, targetAnchors) {
			for (var i = 0; i < sourceAnchors.length; i++) {
				var sourceUUID = toId + sourceAnchors[i];
				plumbInstance.addEndpoint(toId, sourceEndpoint, { anchor:sourceAnchors[i], uuid:sourceUUID });						
			}
			for (var j = 0; j < targetAnchors.length; j++) {
				var targetUUID = toId + targetAnchors[j];
				plumbInstance.addEndpoint(toId, targetEndpoint, { anchor:targetAnchors[j], uuid:targetUUID });						
			}
		};

	var _addNode = function(content, parentElem, id){
		id = id || -1;
		var newId = Math.random().toString(36).substring(7);
		parentElem.append('<div class="window" id="'+newId+'" data-id="'+id+'"><strong>'+content+'</strong><br/><br/></div>');	
		plumbInstance.draggable($('#'+newId));
		_addEndpoints(newId, ["BottomCenter"], ["TopCenter"]);

		return newId;
	};

	addNode = _addNode;

	var _initEndpoints = function(){
		var start = $('#start'),
			end = $('#end');
		plumbInstance.draggable(start);
		plumbInstance.draggable(end);
		_addEndpoints('start', ["BottomCenter"], []);
		_addEndpoints('end', [], ["TopCenter"]);
	};

	initEndpoints = _initEndpoints;


	// suspend drawing and initialise.
	plumbInstance.doWhileSuspended(function() {			
		// listen for new connections; initialise them the same way we initialise the connections at startup.
		plumbInstance.bind("connection", function(connInfo, originalEvent) { 
			init(connInfo.connection);
		});
					
		// make all the window divs draggable						
		plumbInstance.draggable($(".flowchart-demo .window") , { grid: [20, 20] });

		//Right click window deletion
		$(".flowchart-demo")
		.on('contextmenu', '.window', function(e){
			if (confirm("Delete question?")){
				plumbInstance.detachAllConnections(this.id);
				plumbInstance.removeAllEndpoints(this.id);
				this.remove();
			}
			e.preventDefault();
		});

		plumbInstance.bind("contextmenu", function(conn, originalEvent) {
			if(typeof(conn.sourceId) !== "undefined" && typeof(conn.targetId) !== "undefined" ){
				if (confirm("Delete connection from " + $(conn.source).text() + " to " + $(conn.target).text() + "?"))
					jsPlumb.detach(conn); 
				originalEvent.preventDefault();
			}
		});	
		
		plumbInstance.bind("connectionDrag", function(connection) {
			console.log("connection " + connection.id + " is being dragged. suspendedElement is ", connection.suspendedElement, " of type ", connection.suspendedElementType);
		});		
		
		plumbInstance.bind("connectionDragStop", function(connection) {
			console.log("connection " + connection.id + " was dragged");
		});

		plumbInstance.bind("connectionMoved", function(params) {
			console.log("connection " + params.connection.id + " was moved");
		});
	});
}