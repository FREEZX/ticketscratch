$(function(){
	App = Ember.Application.create();

	var currentPage = 1;
	var perPage = 20;

	App.Store = DS.Store.extend({
		revision: 1,
		
		adapter: DS.RESTAdapter.extend({
			url: "http://127.0.0.1/",
			namespace: 'ticket/index.php'
		})
	});

	App.Router.map(function(){
		this.resource('newTicket');
		this.resource('myTickets');
		this.resource('ticket', {path: '/ticket/:ticketid'});
	});

	App.Ticket = DS.Model.extend({
		info: DS.attr('string'),
		priority: DS.attr('string'),
		status: DS.attr('string'),
		time_created: DS.attr('date'),
		time_updated: DS.attr('date'),
		user_email: DS.attr('string'),
		user_id: DS.attr('number')
	});


	//Index
	App.IndexRoute = Em.Route.extend({
		redirect : function(){
			this.transitionTo('tickets');
		}
	});


	//Tickets

	App.MyTicketsController = Ember.ArrayController.extend({
		page: 1,
		perPage: 30,

		getMore: function(){
			if (this.get('loadingMore')) return;
				this.set('loadingMore', true);

			this.get('target').send('getMore');
		},

		// Also add a method `gotMore` that the route can call back to
		// notify the controller that the new data is in and it can stop
		// showing its loading indicator
		gotMore: function(items, page) {
			this.set('page', page);
			var controller = this;
			items.then(function(data){
				controller.set('content', controller.store.all('ticket'));
				controller.set('loadingMore', false);
				controller.refreshHasMore();
			});
		},

		refreshHasMore: function() {
			if(this.store.metadataFor("ticket").total <= this.page*this.perPage ){
				this.set('hasMore', false);
			}
			else{
				this.set('hasMore', true);
			}
		}
	});

	App.MyTicketsRoute = Ember.Route.extend({
		model: function(params){
			return this.store.find('ticket', {limit: 30});
		},

		setupController: function(controller, data) {
			controller.set('model', data);
			this.get('controller').refreshHasMore();
		},

		actions: {
			getMore: function() {
				var controller = this.get('controller'),
				nextPage = controller.get('page') + 1,
				perPage = controller.get('perPage'),
				items;

				items = this.fetchPage(nextPage, perPage);
				// alert the controller to the new data
				controller.gotMore(items, nextPage);
			},
			rerender: function(){
				this.render();
			},
			openDetails: function(id){
				this.transitionTo('ticket', id);
			}
		},
		// load another page's worth of fake widgets
		fetchPage: function(page, perPage) {
			var controller = this.get('controller');
			var args = {
				limit: controller.perPage, 
				skip: (controller.page++) * controller.perPage
			};
			return this.store.find('ticket', args);
		}
	});	

	//Tickets Details
	App.TicketRoute = Em.Route.extend({
		model: function(params){
			return this.store.find('ticket', params.ticketid);
		},
		setupController: function(controller, model){
			controller.set('model', model);
			model.reload();
		}
	});

	App.TicketController = Em.ObjectController.extend({
		actions: {
			addComment: function(){
				var comment = $('#newComment').val();
			}
		}
	});




	//Handlebars helpers
	Ember.Handlebars.registerBoundHelper('date', function(value, options){
		return new moment(value).fromNow();
	});
});